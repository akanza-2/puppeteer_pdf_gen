#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `puppeteer_pdf_gen` package."""
import os

import magic
from django.conf import settings

settings.configure()
settings._wrapped.PUPPETEER_PDF = {}


from puppeteer_pdf_gen import pdf


def test_pdf_conversion_works_correctly():
    puppeteer_instance = pdf.PuppeteerPDF()
    returned_value = puppeteer_instance.print_pdf(url='https://google.com')
    assert isinstance(returned_value, str)


def test_result_file_is_pdf():
    puppeteer_instance = pdf.PuppeteerPDF()
    returned_value = puppeteer_instance.print_pdf(url='https://google.com')
    assert 'PDF document' in magic.from_file(returned_value)


def test_file_can_be_saved_in_specific_directory():
    puppeteer_instance = pdf.PuppeteerPDF()
    puppeteer_instance.print_pdf(
        url='https://google.com',
        file_name='test_specific_file',
        destination_path='/tmp'
    )
    assert os.path.isfile('/tmp/test_specific_file.pdf')
