const puppeteer = require('puppeteer');

const args = process.argv.slice(2);

if (!(!!args) || args.length < 3) {
    console.log('Unsufficient arguments');
} else {
    let [chromePath, url, path, format, landscape, displayHeaderFooter] = args;
    (async () => {
        const browser = await puppeteer.launch({executablePath: chromePath, args: ['--no-sandbox', '--disable-setuid-sandbox']});
        const page = await browser.newPage();
        await page.goto(url, {waitUntil: 'networkidle2'});
        await page.pdf({path, format, landscape, displayHeaderFooter});

        await browser.close();
    })();
}
