# -*- coding: utf-8 -*-
import os
import subprocess
from uuid import uuid4

from django.conf import settings
from django.template.defaultfilters import yesno

PUPPETEER_PDF_GENERATOR_SETTINGS = settings.PUPPETEER_PDF
DEFAULT_WORKSPACE_DIRECTORY_PATH = os.path.dirname(os.path.realpath(__file__))


class PuppeteerPDF(object):
    workspace_directory_path = settings.PUPPETEER_PDF.get('workspace_directory_path', DEFAULT_WORKSPACE_DIRECTORY_PATH)
    destination_path = settings.PUPPETEER_PDF.get('default_destination_path', '/tmp')
    node_executable_path = settings.PUPPETEER_PDF.get('node_path', 'node')
    custom_script_path = settings.PUPPETEER_PDF.get('custom_script', os.path.join(DEFAULT_WORKSPACE_DIRECTORY_PATH, 'pdf.js'))
    chrome_path = settings.PUPPETEER_PDF.get('chrome_path', '/usr/bin/google-chrome')

    def __init__(self, destination_path=None):
        """
        PuppeteerPDF constructor
        :param chunk_size:
        :param destination_path:
        """
        self.destination_path = destination_path if destination_path is not None else self.destination_path

    def print_pdf(self, url, file_name=None, destination_path=None, print_format='A4', orientation=False,
                  header_and_footer=False):
        """
        Simple function for printing PDF from an url.
        :param url: Url to print
        :param file_name: Optional filename, if none filename will be random
        :param destination_path: Base file_path for the file to be saved. If None, default will be used
        :param print_format: Puppeteer document format string. A4 as default
        :param orientation: Puppeteer orientation. False - portrait, True - landscape
        :param header_and_footer: Declare if there are header and footers in document

        :return: String with file path
        """
        file_name = self.prepare_filename(file_name)
        file_path = self.prepare_filepath(file_name, destination_path)

        command = self.build_command(url=url,
                                     file_path=file_path,
                                     print_format=print_format,
                                     orientation=orientation,
                                     header_and_footer=header_and_footer)
        subprocess.call(command)

        return file_path

    def prepare_filename(self, file_name):
        if file_name is None:
            file_name = uuid4().hex
        return file_name + '.pdf'

    def prepare_filepath(self, file_name, destination_path):
        destination_path = self.destination_path if destination_path is None else destination_path
        return os.path.join(destination_path, file_name)

    def build_command(self, url, file_path, print_format='A4', orientation=False, header_and_footer=False):
        """
        Builds Node command for subprocess.
        :param url:
        :param file_path:
        :param print_format:
        :param orientation:
        :param header_and_footer:
        :return: Array describing subprocess command
        """
        command = [
            self.node_executable_path,
            self.custom_script_path,
            self.chrome_path,
            url,
            file_path,
            print_format,
            yesno(orientation, 'true,false'),
            yesno(header_and_footer, 'true,false')
        ]
        return command
