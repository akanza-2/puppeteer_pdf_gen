# -*- coding: utf-8 -*-

"""Top-level package for Django PDF Generator."""

__author__ = """Wojciech Prokop"""
__email__ = 'wojciech.prokop@akanza.pl'
__version__ = '0.0.1'
