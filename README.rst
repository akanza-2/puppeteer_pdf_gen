====================
Django PDF Generator
====================




Django PDF Generator with Puppeteer


* Free software: MIT license


Usage
--------

Construct base class:

.. code-block:: python

    from puppeteer_pdf_generator.pdf import PuppeteerPDF


    puppeteer_instance = PuppeteerPDF(chunk_size=1024, destination_path='/tmp')

Then run the Puppeteer conversion:

.. code-block:: python

    puppeteer_instance.print_pdf(url='https://google.com')

PuppeteerPDF.print_pdf method should return iterable tuple with file chunks (size specified in settings or constructor)

**Available settings in Django:**

All variables should be in *PUPPETEER_PDF* dictionary constant, see following example:

.. code-block:: python

    PUPPETEER_PDF = {
        'workspace_directory_path': 'path_to_directory_with_pdf.js_file',
        'default_destination_path': '/tmp',
        'node_path': 'node',
        'custom_script': 'path_to_pdf.js_file',
        'default_chunk_size': 1024
    }

Credits
-------

Functionality briefly created for another project.

Restructured by Wojciech Prokop.

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
